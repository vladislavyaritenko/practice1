package ua.nure.yaritenko.Practice1;

/**
 * Created by student on 26.10.2017.
 */
public class Part4 {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int sum = 0;
        while (a != 0) {
            sum = sum + (a % 10);
            a /= 10;
        }
        System.out.println("Summa a = " + sum);
    }
}
