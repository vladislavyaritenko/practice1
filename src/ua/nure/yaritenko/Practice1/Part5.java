package ua.nure.yaritenko.Practice1;

public class Part5 {

    public static String digits2chars(int number){
        String result = "";
        if (number > 0)
        {
            int alphabets = (number - 1) / 26;
            int remainder = (number - 1) % 26;
            result = String.valueOf((char)('A' + remainder));
            if (alphabets > 0)
                result = digits2chars(alphabets) + result;
        }
        else
            result = null;
        return result;
    }

    public static double chars2digits(String number) {
        String a1 = number.toUpperCase();
        char[] b = a1.toCharArray();
        int[] mas = new int[a1.length()];
        double sum = 0.0;
        for (int i = 0; i < mas.length; i++) {
            mas[i] = ((int) b[i]) - 64;
        }
        for (int j = mas.length - 1, i = 0; j >= 0; i++, j--) {
            double sum1 = Math.pow(26, j) * mas[i];
            sum += sum1;
        }
        return sum;
    }

    public static String rightColumn(String number) {
        int result = (int)(chars2digits(number)) + 1;
        String Result = digits2chars(result);
        return Result;
    }

    public static void main(String[] args) {
        //A ==> 1 ==> A
        System.out.println(chars2digits("A"));
        System.out.println(digits2chars(1));
        System.out.println(rightColumn("A"));

        System.out.println();
        //B ==> 2 ==> B
        System.out.println(chars2digits("B"));
        System.out.println(digits2chars(2));
        System.out.println(rightColumn("B"));

        System.out.println();
        //Z ==> 26 ==> Z
        System.out.println(chars2digits("Z"));
        System.out.println(digits2chars(26));
        System.out.println(rightColumn("Z"));

        System.out.println();
        //AA ==> 27 ==> AA
        System.out.println(chars2digits("AA"));
        System.out.println(digits2chars(27));
        System.out.println(rightColumn("AA"));

        System.out.println();
        //AZ ==> 52 ==> AZ
        System.out.println(chars2digits("AZ"));
        System.out.println(digits2chars(52));
        System.out.println(rightColumn("AZ"));

        System.out.println();
        //BA ==> 53 ==> BA
        System.out.println(chars2digits("BA"));
        System.out.println(digits2chars(53));
        System.out.println(rightColumn("BA"));

        System.out.println();
        //ZZ ==> 702 ==> ZZ
        System.out.println(chars2digits("ZZ"));
        System.out.println(digits2chars(702));
        System.out.println(rightColumn("ZZ"));

        System.out.println();
        //AAA ==> 703 ==> AAA
        System.out.println(chars2digits("AAA"));
        System.out.println(digits2chars(703));
        System.out.println(rightColumn("AAA"));
    }
}
