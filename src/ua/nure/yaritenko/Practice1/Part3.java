package ua.nure.yaritenko.Practice1;

/**
 * Created by student on 26.10.2017.
 */
public class Part3 {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        while (a != 0 & b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
        }
        System.out.println("NOD = " + (a + b));
    }
}
