package ua.nure.yaritenko.Practice1;

/**
 * Created by student on 26.10.2017.
 */
public class Demo {
    public static void main(String[] args) {
        System.out.println("==== Part 1");
        Part1.main(new String[]{});

        System.out.println("==== Part 2");
        Part2.main(new String[]{"5", "10"});

        System.out.println("==== Part 3");
        Part3.main(new String[]{"12", "36"});

        System.out.println("==== Part 4");
        Part4.main(new String[]{"123"});

        System.out.println("==== Part 5");
        Part5.main(new String[]{});
        //Part5.main(new String[]{"2", "BA", "A"});
    }
}
